# Gitlab-CI first homework
## Table of contents 
1. Dockerfile - content with comments
2. nginx.conf - changing nginx port
3. gitlab-ci.yml - content with comments
4. Workflow
## Dockerfile - content with comments 
```
# Using ubuntu 20.04
FROM ubuntu:20.04

# Information about me
MAINTAINER denys naida <NaidaDV@nmu.one>

# Updating, upgrading && installing reqired soft
RUN apt-get update && apt-get upgrade -y && apt-get install nginx -y && apt-get install net-tools -y && apt-get install openjdk-11-jdk -y && apt-get install sudo -y && apt-get install curl -y

# Adding user with privileges
RUN useradd -s /bin/bash -d /home/main_admin -m --no-log-init main_admin && usermod -aG sudo main_admin
RUN echo "main_admin:madmin" | chpasswd

# Adding user for nginx
RUN groupadd nginx_admin && useradd -g nginx_admin -s /bin/bash -d /etc/nginx -m  --no-log-init nginx_admin && usermod -aG www-data main_admin
RUN echo "nginx_admin:nadmin" | chpasswd

# Changing nginx.conf
RUN rm /etc/nginx/nginx.conf
COPY ./nginx.conf /etc/nginx/nginx.conf

#Adding rights to nginx_user
RUN chown -R nginx_admin /etc/nginx/ && chown -R nginx_admin /var/log/nginx/ && chown -R nginx_admin /var/lib/nginx/ && chown -R nginx_admin /run
RUN chmod -R u+w /etc/nginx/ && chmod -R u+w /var/log/nginx/ && chmod -R u+w /var/lib/nginx/ && chmod -R u+w /run

USER nginx_admin
CMD ["nginx", "-g", "daemon off;"]
```
## nginx.conf - changing nginx port
For changing default nginx port I have added this directive to http:
```
server {
        listen 8081;
        listen [::]:8081;

        server_name localhost 127.0.0.1;
        root /usr/share/nginx/html;
    }
```
So, now nginx working on port 8081, broadcasting it's default index.html
## gitlab-ci.yml - content with comments
```  
image: docker:dind

# Starting docker in docker, adding to service alias - localhost for testing image
services:
  - name: docker:dind
    alias: localhost

variables:
  img_name: my-image
  container_name: my-container

stages:
  - build
  - test
  - push

# Building image from dockerfile, saving image as artifact
Building image:
  stage: build
  script:
    - echo "Building image from Dockerfile"
    - >
      docker build
      --tag "${img_name}":"$CI_PIPELINE_ID"
      .
    - docker save "${img_name}":"$CI_PIPELINE_ID" > ${img_name}.tar
  artifacts:
    paths:
      - ${img_name}.tar

# Preinstalling wget and curl for tests, running container from created image, testing 
Testing image:
  stage: test
  before_script:
    - apk add --update wget && rm -rf /var/cache/apk/*
    - apk add --update curl && rm -rf /var/cache/apk/*
  script:
    - echo "Testing nginx, running in container with curl"
    - docker load < ${img_name}.tar
    - docker run -itd -p 8081:8081 --name ${container_name} ${img_name}:"$CI_PIPELINE_ID"
    - wget -S localhost:8081
    - curl localhost:8081

# Loging in GitLab's project registry, changing image tag for pushing, pushing image like <name>:<pipeline_id> and <name>:<latest>
Pushing image:
  stage: push
  script:
    - echo "Pushing image to GitLab registry"
    - docker load < ${img_name}.tar
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker image tag "${img_name}":"$CI_PIPELINE_ID" registry.gitlab.com/naidadv/gitlab-ci-first-homework:"$CI_PIPELINE_ID"
    - docker image tag "${img_name}":"$CI_PIPELINE_ID" registry.gitlab.com/naidadv/gitlab-ci-first-homework:latest
    - docker push registry.gitlab.com/naidadv/gitlab-ci-first-homework:"$CI_PIPELINE_ID"
    - docker push registry.gitlab.com/naidadv/gitlab-ci-first-homework:latest
```
## Workflow

There are 3 stages:

![](screenshots/1.png)

"Nginx is running inside container" proofs:

![](screenshots/2.png)

![](screenshots/3.png)

Also homework include runtime_output.txt where you can find full successful output from GitLab runtime.
GitLab registry is: https://gitlab.com/NaidaDV/gitlab-ci-first-homework/container_registry
